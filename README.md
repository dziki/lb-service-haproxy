[![pipeline status](https://gitlab.com/dziki/lb-service-haproxy/badges/master/pipeline.svg)](https://gitlab.com/dziki/lb-service-haproxy/commits/master)

# HAProxy with custom error pages

Auto updates Rancher dev environment with newest version. 
No custom HAProxy config needed or additional settings, all
you need is default Rancher load balancer stack`load-balancer`
stack with `load-balancer` service.

### Known issues

If `load-balancer` container is not updating to newest version change tag name
or add label 'io.rancher.container.pull_image' with value `always`. 