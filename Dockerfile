FROM rancher/lb-service-haproxy

LABEL maintaner="Michał Mleczko <michal@mleczko.waw.pl>"

COPY /errors /etc/haproxy/errors
